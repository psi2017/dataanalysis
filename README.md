# README #

This README for the datanalysis for the PSI Course.

### Branches? ###

* Master will be the final branch with only working features
* daq is the current branch working on the linux maschin
* daq_feature is the branch for dev. feature for a quick view on the data
* psi_data_converter is the branch for more general features mentained by Ziga 

### How do I get set up? ###

* clone the repo 
* git checkout daq
* cd dataanalyis/psi_data_converter
* mkdir build
* cd build
* cmake ..
* make

### Contribution guidelines ###

* creat a branch or work on daq_feature branch
* creat a pull request for the daq branch if your feature is working 
* every day there will be a review and a merge into the master branch
