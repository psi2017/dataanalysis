from remote_device_managment import mount_device, unmount_device, connect_to_remote_device
import os


class RemoteDevice:
    def __init__(self, name, location, ip):
        self.name = name
        self.location = location
        self.ip = ip
        self.mode = "production"  # can be switched to "debug"

        if self.mode == "debug":
            print("Executing list of commands")

    def execute_command(self, command: str):
        if self.mode == "debug":
            print(command)

        os.system(command)

    def mount_device(self):
        # todo if self.name directory at mounts doesn't exist create it
        command = mount_device(self.name, self.ip, "/home/" + self.name, self.name)
        print("Mounting device {}".format(self.name))
        self.execute_command(command)

    def unmount_device(self):
        command = unmount_device(self.name)
        print("Unmounting device {}".format(self.name))
        self.execute_command(command)

    def unmount_data_folder(self):
        command = unmount_device("data_folder")
        print("Unmounting data_folder.")
        self.execute_command(command)

    def attach_to_tmux(self):
        self.ssh_to_device()
        command: str = ""
        # todo run tmux attach
        self.execute_command(command)

    def mount_data_folder(self):
        command = mount_device(self.name, self.ip, "/mnt/data/psi2017", "data_folder")
        print("Mounting data_folder")
        self.execute_command(command)

    def ssh_to_device(self):
        command = connect_to_remote_device(self.name, self.ip)
        print("Connecting to device {} via ssh.".format(self.name))
        self.execute_command(command)

""" Tmux usage proceduree:
- install tmux
- run tmux
- execute command in tmux
- detach from session
- detach from pi

"""
