from RemoteDevice import RemoteDevice

psi_linux_machine = RemoteDevice("psi2017", "PSI", "mu3esimon.dyndns.org")

psi_linux_machine.unmount_device()
psi_linux_machine.unmount_data_folder()

psi_linux_machine.mount_device()
psi_linux_machine.mount_data_folder()
psi_linux_machine.ssh_to_device()


# todo print that you are mounting and what is each login for

# todo create ssh automatic login files: check if exists if it doesnt set it up


# setting upp ssh conection
""""
source: https://serverfault.com/questions/241588/how-to-automate-ssh-login-with-password

todo: check that ssh key exists and that .ssh file exists

ssh-keygen -t rsa -b 2048
ssh-copy-id psi2017@mu3esimon.dyndns.org # so we have to pass the password only at the time of first connection
ssh psi2017@mu3esimon.dyndns.org
"""


# todo make sure that all devices were unmounted (maybe unmount them first)
#
