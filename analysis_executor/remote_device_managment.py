path_to_mounts = "/Users/ziga/Workspace/mounts"


def connect_to_remote_device(device_name, ip):
    # connect to raspbarry pi
    return "ssh -y {}@{}".format(device_name, ip)


def mount_device(device_name, ip, source_folder, destination_folder):
    # source_folder: /home/pi
    # destinatio_folder: path_to_mounts + "pi/"
    return "sshfs {}@{}:{}  {}/{}/".format(device_name, ip, source_folder,path_to_mounts, destination_folder)


def unmount_device(destination_folder):
    return "diskutil umount {}/{}".format(path_to_mounts, destination_folder)

# script that unmounts all mounted disks before shut down or restart of computer
