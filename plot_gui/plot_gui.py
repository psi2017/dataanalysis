# Use Tkinter for python 2, tkinter for python 3
from tkinter import *
from PIL import Image, ImageTk

class plotGui():
    def __init__(self, master, *args, **kwargs):
        self.master = master

        self.eventTime = Label(self.master, text="Event Time: ")
        self.eventTime.pack()
        self.eventTime.grid(sticky="W", row=0, column=0)

        self.eventNumber = Label(self.master, text="Event Number: ")
        self.eventNumber.pack()
        self.eventNumber.grid(sticky="W", row=1, column=0)

        self.acceptAll = Button(self.master, text="Accept All")
        self.acceptAll.pack()
        self.acceptAll.grid(sticky="W", row = 2, column = 0)

        self.rejectAll = Button(self.master, text="Reject All")
        self.rejectAll.pack()
        self.rejectAll.grid(sticky="W", row=2, column=1)



        for i in range(5):
            eventTime = Label(self.master, text="Channel: " + str(i))
            eventTime.pack()
            eventTime.grid(sticky="W", row=3+i, column=0)

            path = "Plots/wave.png"
            img = Image.open(path)
            photo = ImageTk.PhotoImage(img)
            panel = Label(image=photo)
            panel.image = photo
            panel.pack()
            panel.grid(sticky="W", row=4+i, column=0, columnspan=2, rowspan=2)





if __name__ == "__main__":
    root = Tk()
    plotGui(root)
    root.mainloop()