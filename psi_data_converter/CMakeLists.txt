
#-----| Set cmake |-----------------------------------------------------------------------------------------------------
# sets minimum version of cmake we are using if possible always use the latest one

if (UNIX AND NOT APPLE)
    set(LINUX TRUE)
endif()

if (APPLE)
    cmake_minimum_required(VERSION 3.8)
endif(APPLE)

if (LINUX)
    cmake_minimum_required(VERSION 3.4)
endif(LINUX)

#-----| Set compiler, output folders |----------------------------------------------------------------------------------
project(psi_data_converter)
set(PROJECT_NAME psi_data_converter)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")    # define compiler
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2")           # compiler optimization
set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS}")
set(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} ${GCC_COVERAGE_LINK_FLAGS}" )

# saving output files to current directory
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}")
set(PROJECT_LINK_LIBS ) # storage for all linked libraries we add (at this point empty)

#-----|  Define sources to used files |---------------------------------------------------------------------------------
include_directories(include) # add all header files from the directory
# bring the headers into the build environment
file(GLOB_RECURSE HEADER_PATHS "include/*.h")
# add files that end with .cpp from src directory but use carefully because cmake won't detect newly added files automatically
file(GLOB_RECURSE SOURCE_FILES "src/*.cpp")
#file(GLOB TEST_FILES "tests/")
#set(SOURCE_FILES ${SOURCE_FILES} ${TEST_FILES})

#-----| Check OS |------------------------------------------------------------------------------------------------------
if (APPLE)
    include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/Unix_setup.cmake)
endif(APPLE)

if (LINUX)
    include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/Linux_setup.cmake)
endif (LINUX)

#-----------------------------------------------------------------------------------------------------------------------