#-----| Adding 3rd Party libraries that are used |----------------------------------------------------------------------

#message(${ROOT_INCLUDE_DIRS} " ")
list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)

# locate the ROOT package and defines a number of variables (e.g. ROOT_INCLUDE_DIRS)
find_package(ROOT REQUIRED )


include_directories(SYSTEM "/usr/include/root/")

find_package(Boost REQUIRED COMPONENTS filesystem system)
include_directories( ${Boost_INCLUDE_DIRS} )

#-----| Adding executables |--------------------------------------------------------------------------------------------
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ../) # add executable to bin directory
# requests that an executable with name EXECUTABLE_NAME is to be built using all the parameters
add_executable(${PROJECT_NAME}  ${SOURCE_FILES})
target_link_libraries(${PROJECT_NAME} ${PROJECT_LINK_LIBS})

#-----| Adding linkers for 3rd Party libraries that are used |----------------------------------------------------------

target_link_libraries(${PROJECT_NAME} ${ROOT_LIBRARIES} -lSpectrum ${Boost_LIBRARIES})
#-----------------------------------------------------------------------------------------------------------------------