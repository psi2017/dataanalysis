#-----| Adding 3rd Party libraries that are used |----------------------------------------------------------------------

# path to the root library
set(ROOTSYS  CACHE STRING "ROOT path")
list(APPEND CMAKE_PREFIX_PATH ${ROOTSYS})
# locate the ROOT package and defines a number of variables (e.g. ROOT_INCLUDE_DIRS)
find_package(ROOT REQUIRED )
# define useful ROOT functions and macros (e.g. ROOT_GENERATE_DICTIONARY)
include(${ROOT_USE_FILE})

#find_package(Boost 1.64.0 COMPONENTS system filesystem REQUIRED)
#if(Boost_FOUND)
#      message(STATUS "Boost_INCLUDE_DIRS: ${Boost_INCLUDE_DIRS}")
#      message(STATUS "Boost_LIBRARIES: ${Boost_LIBRARIES}")
#      message(STATUS "Boost_VERSION: ${Boost_VERSION}")
#      include_directories(${Boost_INCLUDE_DIRS})
#endif()


#-----| Adding executables |--------------------------------------------------------------------------------------------
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ../) # add executable to bin directory
# requests that an executable with name EXECUTABLE_NAME is to be built using all the parameters
add_executable(${PROJECT_NAME}  ${SOURCE_FILES})
target_link_libraries(${PROJECT_NAME} ${PROJECT_LINK_LIBS})

#-----| Adding linkers for 3rd Party libraries that are used |----------------------------------------------------------

target_link_libraries(${PROJECT_NAME} ${ROOT_LIBRARIES})

#if(Boost_FOUND)
#       target_link_libraries(${PROJECT_NAME} ${Boost_LIBRARIES})
#endif()
#-----------------------------------------------------------------------------------------------------------------------
