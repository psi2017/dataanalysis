//
// Created by Ziga Brencic on 03/09/2017.
//

#ifndef PSI_DATA_CONVERTER_ANALYST_H
#define PSI_DATA_CONVERTER_ANALYST_H

#include "support_programs/Flags.h"

#include <string>

class Analyst : public Flags {
public:
    unsigned int run_number;
    std::string analysis_type;

    void execute_binary_reader(bool rewrite_files);
    void execute_waveform_analysis(bool rewrite_files);

    Analyst(unsigned int run_number, std::string &analysis_type);
    virtual ~Analyst();

};


#endif //PSI_DATA_CONVERTER_ANALYST_H
