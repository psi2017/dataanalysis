//
// Created by Ziga Brencic on 19/02/2018.
//

#ifndef PSI_DATA_CONVERTER_BASELINEANALYSIS_H
#define PSI_DATA_CONVERTER_BASELINEANALYSIS_H

#include "baseline_analysis/RunInfo.h"

#include "baseline_analysis/Waveform.h"

class BaselineAnalysis {
public:

    std::vector<uint16_t> points;
    double baseline;
    double waveform_average;

    shared_ptr<RunInfo> run_info;
    void remove_lest_significant_bit();
    void calculate_baseline();
    void calculate_waveform_average();
    void analyse_baseline(std::vector<uint16_t> &points);

    BaselineAnalysis(shared_ptr<RunInfo> &run_info);
    virtual ~BaselineAnalysis();

};


#endif //PSI_DATA_CONVERTER_BASELINEANALYSIS_H
