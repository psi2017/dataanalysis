//
// Created by Ziga Brencic on 30/08/2017.
//

#ifndef PSI_DATA_CONVERTER_BINREADER_H
#define PSI_DATA_CONVERTER_BINREADER_H

#include <fstream>
#include "support_programs/definitions.h"
#include "TROOT.h"
#include "binary_file_objects.h"

#include "support_programs/Flags.h"

/* Class mechanics:
 * stay in unit_16 because it is faster to do the waveform analysis first
 * then convert to the values that we actually need
 *
 */

class BinaryFile;
class RunInfo;
class RunAnalysis;
class Waveform;
class BaselineAnalysis;
class WaveformAnalysis;


class BinaryReader : public Flags {
public:
    /// variables:
    string analysis_type;
    vector<BaselineAnalysis> baseline_analyser;
    vector<WaveformAnalysis> waveform_analyser;
    shared_ptr<RunInfo> run_info;
    shared_ptr<BinaryFile> binary_file;
//    shared_ptr<RunAnalysis> run_analysis;

//    file_header_structure_t file_header;
//    file_footer_structure_t *file_footer;

    // object that connects to our binary file and enables us to read data from it

    event_structure_t event;                                    // we allocate the event structure only once

    unsigned int current_event_size;
    uint64_t current_daq_time_stamp_1;
    uint64_t current_daq_time_stamp_2;
    uint64_t stuff;

    vector<Waveform> raw_waveforms;                   // storage for waveform objects for every channel

    vector<Double32_t> list_plot_points;
    vector<uint64_t> baseline;
    vector<uint64_t> waveform_average;

    unsigned int event_number;
    unsigned int num_of_waveforms_from_veto_channel;

    /// functions:

    void load_channel_data_from_stream(unsigned int channel_number);
    void allocate_memory();
    void create_baseline_analysis_root_tree();
    void create_waveform_characteristics_root_tree();

    void view_file_header();
    void read_run_header();

    bool check_event_size_that_was_read();
    void view_event_data();

    void convert_time_stamp();
    void analyse_event();

    bool read_event_data();
    void read_run_footer();

    void read_binary_file();

    BinaryReader(unsigned int run_number, string &analysis_type);
    virtual ~BinaryReader();
};


#endif //PSI_DATA_CONVERTER_BINREADER_H
