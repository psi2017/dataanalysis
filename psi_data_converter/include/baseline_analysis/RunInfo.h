//
// Created by Ziga Brencic on 13/09/2017.
//

#ifndef PSI_DATA_CONVERTER_RUN_STRUCTURE_H
#define PSI_DATA_CONVERTER_RUN_STRUCTURE_H

#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "baseline_analysis/binary_file_objects.h"
#include "support_programs/definitions.h"

#include "support_programs/Flags.h"

class RunInfo : public file_header_structure_t, public file_footer_structure_t, public Flags {
public:
    string converted_root_file_name;
    string processed_root_file_name;

    TFile *output_root_file;
    TTree *baseline_analysis_root_tree;
    TTree *waveform_characteristics_root_tree;

    shared_ptr<Flags> data_analysis_flags;


    unsigned int num_of_successfully_read_events;
    uint64_t run_start_time;
    uint64_t run_stop_time;

    bool file_footer_missing;
    bool last_event_was_cut; // flag that stores if last event was not completed when we are missing file footer


    /// run info that we want to store to JSON structure and then to ROOT file
    json run_info;
    std::map<unsigned int, map<string, double>> baseline;
    std::map<unsigned int, map<string, double>> waveform_average;

    uint32_t *settings;

    uint32_t num_of_pre_samples_for_baseline;



    bool run_info_set; // after all the characteristics have been added

    void create_converted_root_file();

    void save_header(file_header_structure_t file_header);
    void save_footer(file_footer_structure_t *file_footer);

    void create_empty_run_json_structure();
    void store_json_structure_to_root_file();
    void set_run_info_from_binary_file();

    void load_channel_data(string &channel_characteristic, unsigned int channel_number, bool debug);

    RunInfo(unsigned int run_number);
    virtual ~RunInfo();

};

#endif //PSI_DATA_CONVERTER_RUN_STRUCTURE_H
