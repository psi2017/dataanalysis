//
// Created by Ziga Brencic on 09/09/2017.
//

#ifndef PSI_DATA_CONVERTER_WAVEFORM_H
#define PSI_DATA_CONVERTER_WAVEFORM_H

#include "support_programs/definitions.h"

class Waveform {
public:
    std::vector<uint16_t> points;
    double baseline;
    double waveform_average;

    void clean_object();

    Waveform(unsigned int waveform_length);
    virtual ~Waveform();

};


#endif //PSI_DATA_CONVERTER_WAVEFORM_H
