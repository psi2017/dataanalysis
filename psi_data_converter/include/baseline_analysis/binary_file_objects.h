//
// Created by Ziga Brencic on 06/09/2017.
//

#ifndef PSI_DATA_CONVERTER_BINARY_FILE_OBJECTS_H
#define PSI_DATA_CONVERTER_BINARY_FILE_OBJECTS_H

#include <stdint.h> // for uint32_t

// todo combine the a,b uint32_t objects

/// Structure of binary file:
/* ====================================================================================================================
   File Header:
   HEAD	TIME-Start	U64
   RUN number	U32
   NxM (Fix)	2xU32  : specifies the size of our input matrix
   Settings	20xU32

   Event Header:
   EVENT1	0XCAFECAFE	U32
   // two different time stamps
   TIME	U64
   TIME U64
   EVENT#	U32
   STUFF	U64
   Event data:
   NxM (Labview)	2xU32
   SAMPLES	NxMxU16

   Event Header:
   EVENT2	0XCAFECAFE	U32

   File footer:
   FOOTER	0XF000F000	U32
   Total Event#	U32
   TIME-Stop	U64
   Stuff	U32
   END	0XF000F000	U32

 =====================================================================================================================*/

// we define default sizes for our object so our compiler doesn't expand them

const unsigned int max_num_of_channels = 14;
const unsigned int max_number_of_waveform_points = 2048;
const unsigned int size_of_analysis_block = 1000;           // how many  events we analise in one block before we set next output to the screen

const unsigned int header_settings_length = 19;

const uint32_t event_header = 0XCAFECAFE;
const uint32_t file_footer = 0xF000F000;

struct file_header_structure_t{
    //Default size of struct:  (5 * 32 + 20 * 32 ) / 8 = 5 * 4 + 20 * 4 = 100
    uint32_t run_start_time_a;
    uint32_t run_start_time_b;
    uint32_t run_number;
    uint32_t N_num_of_channels;
    uint32_t M_event_length;                                // num_of_sampled points with the digitiser
    uint32_t num_pre_samples;
    uint32_t settings[header_settings_length];
};

const unsigned int fixed_file_header_size = (5 * 32 + 20 * 32 ) / 8 ; // = 100

struct file_footer_structure_t{
    //Default size of struct: 6 * 32 / 8 = 24
    uint32_t footer_start;
    uint32_t total_num_of_events;
    uint32_t time_stop_a;
    uint32_t time_stop_b;
    uint32_t stuff;
    uint32_t end_footer;
};

const unsigned int fixed_file_footer_size = 6 * 32 / 8;  // = 24

struct event_structure_t {
    //Default size of struct: (10 * 32 + ... ) / 8 = 40 + ...
    uint32_t event_start;                                   // we check if it is the correct one
    uint32_t time_stamp_daq_1_a;                            // what is the time format of the event
    uint32_t time_stamp_daq_1_b;
    uint32_t time_stamp_daq_2_a;
    uint32_t time_stamp_daq_2_b;
    uint32_t event_number;
    uint32_t stuff_a;
    uint32_t stuff_b;
    uint32_t N_num_of_channels;
    uint32_t M_event_length;                                // num_of_sampled points with the digitiser

    // first N points of linear matrix first channel, second N points second channel, ...
    uint16_t data_stream_matrix[max_num_of_channels * max_number_of_waveform_points];
};

const unsigned int fixed_event_structure_t_size = (32 * 10)/ 8;  // 40 + N x M (N,M part is added later)

#endif //PSI_DATA_CONVERTER_BINARY_FILE_OBJECTS_H
