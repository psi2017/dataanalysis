//
// Created by Ziga Brencic on 05/12/2017.
//

#ifndef PSI_DATA_CONVERTER_BINARY_OPERATIONS_H
#define PSI_DATA_CONVERTER_BINARY_OPERATIONS_H
#include <stdint.h>

uint64_t join_integers(uint32_t leastSignificantWord, uint32_t mostSignificantWord);

#endif //PSI_DATA_CONVERTER_BINARY_OPERATIONS_H
