//
// Created by Ziga Brencic on 08/09/2017.
//

#ifndef PSI_DATA_CONVERTER_RUNANALYSIS_H
#define PSI_DATA_CONVERTER_RUNANALYSIS_H

#include "TCanvas.h"
#include "support_programs/definitions.h"
#include "TMultiGraph.h"
#include "TH2D.h"
#include "TFile.h"
#include "support_programs/Flags.h"

#include "support_programs/aliases.h"

class WaveformAnalysis;
class RunInfo;


class RunAnalysis : public Flags {
public:
    shared_ptr<RunInfo> run_info;
    shared_ptr<Flags> data_analysis_flags;


    unsigned int run_number;
    unsigned int num_of_channels;
    unsigned int num_of_events_analysed;
    unsigned int event_length;

    string plots_folder_path;
//    Int_t canvas_width;
//    Int_t canvas_height;

    bool plot_all_waveforms;

    bool storage_for_all_waveforms_created;

    TFile *output_root_file;

    vector<vector<shared_ptr<vector<Double32_t>>>> matrix_of_waveform_vectors;
    vector<vector<TGraph *>> matrix_of_TGraphs;

    vector<Double32_t> list_plot_points;

    vector<TMultiGraph *> channel_plots;
    vector<TH2D *> waveform_histograms;
    vector<TH2D *> waveform_histograms_zoom;

    void store_event_waveforms(vector<WaveformAnalysis> event_waveforms, unsigned int event_number);
    void save_plot_of_all_waveforms();

    void create_storage_for_plot_all_waveforms();

    void create_plot_of_all_waveforms_from_root_file();

    RunAnalysis(TFile *input_root_file, unsigned int input_run_number, unsigned int input_num_of_channels, unsigned int input_event_length);
    virtual ~RunAnalysis();

//    gROOT->GetObject("root_tree");

};


#endif //PSI_DATA_CONVERTER_RUNANALYSIS_H
