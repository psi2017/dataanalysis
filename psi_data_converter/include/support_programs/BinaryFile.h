//
// Created by Ziga Brencic on 13/09/2017.
//

#ifndef PSI_DATA_CONVERTER_BINARYFILE_H
#define PSI_DATA_CONVERTER_BINARYFILE_H

#include "definitions.h"
#include <sstream>
#include <iomanip>
#include "support_programs/Flags.h"
#include "support_programs/IO.h"

class BinaryFile : public Flags {
public:
    string file_path;
    string file_name;

    std::fstream stream;
    unsigned int buffer_size;
    bool end_of_file_reached;

    void open_file();
    void close_file();
    void is_end_of_file_reached();

    bool read_buffer_matches_buffer_size();

    template <typename TYPE>
    void read_buffer(TYPE &buffer, unsigned int buffer_size){
        this->buffer_size = buffer_size;
        this->stream.read(reinterpret_cast<char*>(&buffer), this->buffer_size);
        // count how many characters were actually read from the stream if there were any
        size_t count = (size_t)this->stream.gcount();
        if(!count){
            abort_error_massage("Error reading from binary file. Nothing was loaded from the file.");
            // todo be able to print all the output that was read till then
        }
        is_end_of_file_reached();
    }

    BinaryFile(string file_path, string file_name);
    virtual ~BinaryFile();

};


#endif //PSI_DATA_CONVERTER_BINARYFILE_H
