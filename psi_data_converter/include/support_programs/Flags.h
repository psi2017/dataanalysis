//
// Created by Ziga Brencic on 13/09/2017.
//

#ifndef PSI_DATA_CONVERTER_FLAGS_H
#define PSI_DATA_CONVERTER_FLAGS_H


class Flags{
public:
    bool waveform_analysis;
    bool debug;
    bool event_bin_format;
    bool run_analysis;
    bool destructor_debugging;
    bool get_pdf;

    Flags();
    virtual ~Flags();
};

#endif //PSI_DATA_CONVERTER_FLAGS_H
