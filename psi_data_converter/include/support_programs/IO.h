//
// Created by Ziga Brencic on 05/09/2017.
//

#ifndef PSI_DATA_CONVERTER_IO_H
#define PSI_DATA_CONVERTER_IO_H

#include "support_programs/definitions.h"

void massage(string massage_content, char massage_mark );
void print_big_separator(string input_string);
void print_small_separator(string input_string);
void error_massage(string output);
void abort_error_massage(string output);

#endif //PSI_DATA_CONVERTER_IO_H
