//
// Created by Ziga Brencic on 30/08/2017.
//

#ifndef PSI_DATA_CONVERTER_ROOTREADER_H
#define PSI_DATA_CONVERTER_ROOTREADER_H

#include <fstream>
#include "support_programs/definitions.h"
#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "waveform_analysis/WaveformAnalysis.h"

#include "support_programs/Flags.h"

/* Class mechanics:
 * stay in unit_16 because it is faster to do the waveform analysis first
 * then convert to the values that we actually need
 *
 */



class RootReader : public Flags {
public:
    unsigned int pre_samples;
    unsigned int num_of_channels;
    unsigned int run_number;
    unsigned int event_length;

    unsigned int max_num_of_channels;
    unsigned int max_number_of_waveform_points;

    uint64_t time_start;
    uint64_t time_stop;
    uint32_t stuff;
    bool file_footer_missing;
    bool last_event_was_cut;
    unsigned int num_of_pre_samples;
    unsigned int num_of_events;


    TFile *root_file;
    TTree *root_tree;
    json run_info;

    vector<vector<UShort_t>> event_waveforms;

    bool debug_mode;

    string root_file_path;

    void open_root_file_and_connect_to_root_tree();
    void set_branch_addresses();
    void set_aliases();
    void read_from_root_file();
    void memory_allocation();
    void print_run_info();
    void read_run_info();

    // todo write those writers
    void load_un_analysed_waveform();
    void load_analysed_waveform();

    RootReader(unsigned int input_run_number);
    virtual ~RootReader();
};


#endif //PSI_DATA_CONVERTER_ROOTREADER_H
