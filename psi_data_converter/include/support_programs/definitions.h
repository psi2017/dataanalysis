// Created by Ziga Brencic

#ifndef PSI_DATA_CONVERTER_DEFINITIONS_H
#define PSI_DATA_CONVERTER_DEFINITIONS_H

#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <exception>
#include <map>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <memory>
#include <stdint.h>

using namespace std;

#include "json.h"
using json = nlohmann::json;

const unsigned int canvas_width = 1600;
const unsigned int canvas_height = 600;


#endif //PSI_DATA_CONVERTER_DEFINITIONS_H
