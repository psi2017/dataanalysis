//
// Created by Ziga Brencic on 21/02/2018.
//

#ifndef PSI_DATA_CONVERTER_GET_DISTRIBUTION_PEAK_H
#define PSI_DATA_CONVERTER_GET_DISTRIBUTION_PEAK_H

#include "TH1F.h"

struct distribution_peak_t{
    int bin_number;
    double x;
    double y;

    void print(){
        cout << "Distribution peak: (bin_number, x, y) = (" << bin_number << ", " << x  << ", " << y << ")" << endl;
    }
};

distribution_peak_t get_distribution_peak(TH1F *distribution){
    // in case of non symmetric distribution the distribution peak won't correspond to mean of distribution
    distribution_peak_t peak{};
    peak.bin_number = distribution->GetMaximumBin();
    peak.x = distribution->GetXaxis()->GetBinCenter(peak.bin_number);
    peak.y = distribution->GetBinContent(peak.bin_number);

    return peak;
}
#endif //PSI_DATA_CONVERTER_GET_DISTRIBUTION_PEAK_H
