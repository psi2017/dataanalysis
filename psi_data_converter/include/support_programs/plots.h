//
// Created by Ziga Brencic on 16/01/2018.
//

#ifndef PSI_DATA_CONVERTER_PLOTS_H
#define PSI_DATA_CONVERTER_PLOTS_H
#include <stdint.h>
#include "support_programs/definitions.h"
void list_plot(const string &plot_name, const string &plot_path, vector<uint16_t> points);

#endif //PSI_DATA_CONVERTER_PLOTS_H
