//
// Created by Ziga Brencic on 22/11/2017.
//

#ifndef PSI_DATA_CONVERTER_ANALYSEDWAVEFORM_H
#define PSI_DATA_CONVERTER_ANALYSEDWAVEFORM_H

#include "support_programs/definitions.h"

const unsigned int max_number_of_waveform_peaks = 10;

class AnalysedWaveform {
public:
    std::vector<uint16_t> points;
    std::vector<uint16_t> derivatives;

    std::vector<uint16_t> peak_height;
    std::vector<uint64_t> peak_position;
    std::vector<uint64_t> rise_edge_position;
    std::vector<uint64_t> fall_edge_position;

    uint32_t noise;
    std::vector<uint64_t> integral;
    std::vector<uint64_t> integration_interval; // waveform_end - waveform_start

    uint32_t threshold_level;

    bool flat;
    bool rise_edge_problem;
    bool fall_edge_problem;
    bool multiple_peaks;

    AnalysedWaveform(unsigned int input_N_num_of_points);
    virtual ~AnalysedWaveform();

};



#endif //PSI_DATA_CONVERTER_ANALYSEDWAVEFORM_H
