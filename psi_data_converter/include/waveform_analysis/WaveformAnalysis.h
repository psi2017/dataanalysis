//
// Created by Ziga Brencic on 01/09/2017.
//

#ifndef PSI_DATA_CONVERTER_WAVEFORMANALYSIS_H
#define PSI_DATA_CONVERTER_WAVEFORMANALYSIS_H

#include "support_programs/definitions.h"

#include <cstdint>
#include <vector>
#include "TGraph.h"
#include "TFile.h"

#include "support_programs/Flags.h"

class RunInfo;
class Waveform;


// gets a vector with waveform data and analyses it
class WaveformAnalysis : public Flags {
public:
    unsigned int event_number;
    unsigned int channel_number;
    unsigned int num_of_channels;
    unsigned int num_of_peaks;


    vector<uint16_t> waveform_points;
    vector<uint16_t> waveform_derivatives;

    uint32_t baseline;

    uint64_t peak_height;

    // todo make sure that we save that
    vector<uint64_t> integration_intervals; // waveform_end - waveform_start


    uint64_t rise_edge_position;

    shared_ptr<TGraph> waveform_plot;
    shared_ptr<TFile> output_root_file;

    shared_ptr<RunInfo> run_info;
    shared_ptr<Flags> data_analysis_flags;

	unsigned int num_of_channel_waveforms_to_plot;

    // flags
	bool rising_edge_problem;
    bool falling_edge_problem;

    void calculate_channel_baseline(unsigned int num_of_pre_samples_for_baseline);
    void set_channel_baseline(unsigned int num_of_pre_samples_for_baseline);

	void calculate_waveform_derivatives();
    uint64_t find_rise_edge_by_threshold();
    uint64_t find_integral_by_fix_window(uint64_t rising_edge);

 
    uint64_t integration_width_trapezoidal_method();

    void process_peaks_by_noise_threshold();
    void process_peaks_by_DOS();
    void process_peaks_root();
    void find_amplitude();
    void integrate_peaks(); // integral = sum - \Delta t noise \Delta t
    void integrate_peaks_constant_interval();


    void rise_edge_detection();

    void multiple_peak_finder();

    void clear_storage();

    void plot_waveform(vector<Double_t> list_plot_points);
    void analyse_waveform(unsigned int channel_number, unsigned int event_number, Waveform &raw_waveform);
    void save_waveform();
    WaveformAnalysis(shared_ptr<RunInfo> run_info);
    virtual ~WaveformAnalysis();
};


#endif //PSI_DATA_CONVERTER_WAVEFORMANALYSIS_H
