//
// Created by Ziga Brencic on 21/02/2018.
//

#ifndef PSI_DATA_CONVERTER_WAVEFORM_CHARACTERISTICS_H
#define PSI_DATA_CONVERTER_WAVEFORM_CHARACTERISTICS_H

#include <vector>
#include <map>
#include <stdio.h>
#include <string>
using namespace std;

struct waveform_characteristics_t{
    map<string, vector<double>> rise_edge; // ch 1 - 13
    map<string, vector<UInt_t>> peak_position; // ch 0 - 13

    map<string, vector<double>> fall_edge; // ch 7 - 8
    map<string, vector<double>> integral; // ch 7 - 8
};
#endif //PSI_DATA_CONVERTER_WAVEFORM_CHARACTERISTICS_H
