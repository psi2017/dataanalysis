#! /bin/bash
# this script is used to automate the setup of the converter on the remote machine
mkdir -p build
cd build
cmake ..
make
cd ..
mkdir -p converted_runs
mkdir -p plots_final
mkdir -p processed_runs_final
mkdir -p binary_files
mkdir -p calibrated_runs

if [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]
then
  # we do the symilinking on linux remote machine since our data is on location:
  data_location="/home/psi2017/data/preliminary"
  echo "Please varify that data is stored at: $data_location"
  # move to data direcotry 
  cd /home/psi2017/data/preliminary/
  mkdir -p converted_runs
  mkdir -p plots_final
  mkdir -p processed_runs_final
  mkdir -p binary_files
  mkdir -p calibrated_runs

  # do the symilinking
  ln -sf /home/psi2017/data_analysis_ziga/psi_data_converter/psi_data_converter /home/psi2017/bin/converter_ziga

  # move back to the location where the script is installed
  cd /home/psi2017/data_analysis_ziga/psi_data_converter
fi
