//
// Created by Ziga Brencic on 03/09/2017.
//

#include "Analyst.h"
#include "support_programs/IO.h"
#include "baseline_analysis/BinaryReader.h"
#include "support_programs/RootReader.h"

#include "baseline_analysis/RunInfo.h"

void Analyst::execute_binary_reader(bool rewrite_files) {
    bool file_exists = false; // todo make sure that analysed file exists or not
    if(rewrite_files and !file_exists) {
        shared_ptr<BinaryReader> reader = shared_ptr<BinaryReader>(new BinaryReader(run_number, analysis_type));
    }
}


void Analyst::execute_waveform_analysis(bool rewrite_files) {
    bool file_exists = false; // todo make sure that analysed file exists or not
    if(rewrite_files and !file_exists) {

        // loading the baseline data
        auto run_info = shared_ptr<RunInfo>(new RunInfo(run_number));
        bool debug = false;
        string baseline = "baseline";
        string waveform_average = "waveform_average";
        for(unsigned int channel_number = 0; channel_number < 14; ++channel_number) {
            run_info->load_channel_data(baseline, channel_number, debug);
            run_info->load_channel_data(waveform_average, channel_number, debug);
        }

//        shared_ptr<BinaryReader> reader = shared_ptr<BinaryReader>(new BinaryReader(run_number, analysis_type));
        // load waveforms for specific event with binary reader and analyse them

        // create a vector of waveforms

        // analyse each channel: for loop over channels

        // analyse each waveform: loop over all entries for each branch

    }
}

Analyst::Analyst(unsigned int run_number, string &analysis_type) {
    this->run_number = run_number;
    this->analysis_type = analysis_type;
    print_big_separator("Starting " + analysis_type + " of run: " + to_string(run_number));
}

Analyst::~Analyst() {
    if(destructor_debugging)
        cout << "Cleaning Analyst object" << endl;
}