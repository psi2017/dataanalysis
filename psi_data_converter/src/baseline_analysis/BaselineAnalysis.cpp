//
// Created by Ziga Brencic on 19/02/2018.
//

#include "baseline_analysis/BaselineAnalysis.h"

void BaselineAnalysis::remove_lest_significant_bit() {
    //divide all data samples by 4 because we need to get rid of least significant bit since we had a problem in DAQ
    for (uint16_t point = 0; point < run_info->M_event_length; ++point) {
        points.at(point) = (uint16_t)(points.at(point)/4);
    }
}

void BaselineAnalysis::calculate_baseline() {
    for (uint16_t point = 0; point < run_info->num_of_pre_samples_for_baseline ; ++point)
        baseline += (double_t)points.at(point);

    baseline = baseline/((double_t)run_info->num_of_pre_samples_for_baseline);

}

void BaselineAnalysis::calculate_waveform_average() {
    for (uint32_t point = 0; point < run_info->M_event_length ; ++point)
        waveform_average += points.at(point);
    waveform_average = (double_t)waveform_average/((double_t)run_info->M_event_length);
}

void BaselineAnalysis::analyse_baseline(std::vector<uint16_t> &points) {
    this->baseline = 0;
    this->waveform_average = 0;
    this->points = points;
    this->remove_lest_significant_bit();
    this->calculate_baseline();
    this->calculate_waveform_average();
}

BaselineAnalysis::BaselineAnalysis(shared_ptr<RunInfo> &run_info) {
    this->run_info = run_info;
}

BaselineAnalysis::~BaselineAnalysis() = default;
