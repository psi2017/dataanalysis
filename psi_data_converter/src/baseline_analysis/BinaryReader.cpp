//
// Created by Ziga Brencic on 30/08/2017.
//

#include "baseline_analysis/BinaryReader.h"
#include "support_programs/IO.h"
#include "baseline_analysis/binary_operations.h"

#include "data_analysis/RunAnalysis.h"
#include "baseline_analysis/Waveform.h"
#include "support_programs/BinaryFile.h"
#include "support_programs/plots.h"
#include "baseline_analysis/RunInfo.h"
#include "baseline_analysis/BaselineAnalysis.h"
#include "waveform_analysis/WaveformAnalysis.h"

void BinaryReader::allocate_memory(){
    current_event_size = fixed_event_structure_t_size + 2*run_info->N_num_of_channels*run_info->M_event_length;

    raw_waveforms.reserve(run_info->N_num_of_channels); // reserve space for the vector of WaveformAnalysis objects

    // todo why do we need those two? for list plots?
    baseline.assign(run_info->N_num_of_channels,0);
    waveform_average.assign(run_info->N_num_of_channels, 0);


    //  create object for every entry of raw_waveforms vector
    for (int i = 0; i < run_info->N_num_of_channels; ++i) {
        raw_waveforms.emplace_back(run_info->M_event_length);

        if (analysis_type == "baseline_analysis")
            baseline_analyser.emplace_back(run_info);
        else if (analysis_type == "waveform_characteristics_analysis")
            waveform_analyser.emplace_back(run_info);

    }

    for (unsigned int i = 0; i < run_info->M_event_length; ++i)
        list_plot_points.push_back(i);

    run_info->num_of_pre_samples_for_baseline = run_info->num_pre_samples/2;
}

void BinaryReader::create_baseline_analysis_root_tree(){
    /* ROOT file structure: Converted_run_0001.root

    Tree:
        -> ch_01_baseline
        -> ch_01_average
        -> ch_02_baseline
        -> ch_02_average

   TString: contains run time, num_of_events, run_number, attenuation,
   baseline for each channel and the res of the run info that is part of the run info class
    */
    run_info->baseline_analysis_root_tree = new TTree("baseline_analysis_root_tree", "waveforms baseline and waveform average for all channels");


    for (unsigned int channel_number = 0; channel_number < run_info->N_num_of_channels; ++channel_number) {
        string ch_branch_name = "ch_" +  to_string(channel_number);

        string branch_name = ch_branch_name + "_baseline";
        run_info->baseline_analysis_root_tree->Branch(branch_name.c_str(), &baseline_analyser.at(channel_number).baseline, "baseline/D");

        branch_name = ch_branch_name + "_waveform_average";
        run_info->baseline_analysis_root_tree->Branch(branch_name.c_str(), &baseline_analyser.at(channel_number).waveform_average, "wavefrom_avergae/D");

    }

}

// todo start using this function
void BinaryReader::create_waveform_characteristics_root_tree() {
        /* ROOT file structure: Processed_run_0001.root

    Tree:
        -> ch_01_peak_position
        -> ch_01_peak_height
        -> ch_01_integral
        -> ch_01_edge_point
        -> event_time_stamp
        -> event_number
        -> stuff(extra data)

   TObject: run time, num_of_events, run_number and the res of the run data

    */
    run_info->waveform_characteristics_root_tree = new TTree("waveform_characteristics_root_tree", "waveforms characteristics for all channels");


    for (unsigned int channel_number = 0; channel_number < run_info->N_num_of_channels; ++channel_number) {
        string ch_branch_name = "ch_" + to_string(channel_number);
        // todo define branches: num_of_peaks, peak_pos, ...

    }

    string branch_time_stamp_daq_1 = "time_stamp_daq_1";
    string branch_time_stamp_daq_2 = "time_stamp_daq_2";
    string event_number = "event_number";

    run_info->waveform_characteristics_root_tree->Branch(branch_time_stamp_daq_1.c_str(), &current_daq_time_stamp_1, "time_stamp/l");
    run_info->waveform_characteristics_root_tree->Branch(branch_time_stamp_daq_2.c_str(), &current_daq_time_stamp_2, "time_stamp/l");
    run_info->waveform_characteristics_root_tree->Branch(event_number.c_str(), &event.event_number, "event_number/i");

}

void BinaryReader::view_file_header() {
    print_small_separator("Run header:");
    cout << "Run start time:    " << std::hex << run_info->run_start_time << endl;
    cout << "Run number:        " << std::dec << run_info->run_number  << endl;
    cout << "N_num_of_channels: " << std::dec << run_info->N_num_of_channels << endl;
    cout << "M_event_length:    " << std::dec << run_info->M_event_length << endl;
    cout << "Settings: " << endl;
    for (unsigned int j = 0; j < header_settings_length; ++j) cout << std::hex << run_info->settings[j];

    cout << endl;
    cout << string(100, '-')  << endl;
}

void BinaryReader::read_run_header() {
//    const unsigned int header_size = 100; // fixed size for the header
    file_header_structure_t file_header{};

    binary_file->read_buffer(file_header, fixed_file_header_size); // reads the header

    run_info->save_header(file_header);

    if(debug)
        view_file_header();

    if(run_info->N_num_of_channels == 0 or run_info->M_event_length == 0)
        abort_error_massage("We have " + to_string(run_info->N_num_of_channels) + " channels and events of length " +
                              to_string(run_info->M_event_length) + ". Correct this.");

}

void BinaryReader::view_event_data() {
    cout << "Event header:      " << std::hex << event.event_start << endl;
    cout << "Event:             " << std::dec << event.event_number << endl;
    cout << "Time stamp DAQ 1:  " << current_daq_time_stamp_1 << endl;
    cout << "Time stamp DAQ 2:  " << current_daq_time_stamp_2 << endl;
    stuff = join_integers(event.stuff_a, event.stuff_b);
    cout << "Stuff:             " << stuff << endl;
    cout << "N_num_of_channels: " << std::dec << event.N_num_of_channels << endl;
    cout << "M_event_length:    " << std::dec << event.M_event_length << endl;
    cout << string(100, '-') << endl;

    if(event_bin_format){
        for (unsigned int channel_number = 0; channel_number < run_info->N_num_of_channels; ++channel_number) {
            cout << "ch" << setfill('0') << setw(2) << channel_number << " ";
        }
        cout << endl;
        cout << string(100, '-') << endl;

        for (unsigned int event_position = 0; event_position < run_info->M_event_length; ++event_position) {
            for (unsigned int channel_number = 0; channel_number < run_info->N_num_of_channels; ++channel_number) {
                unsigned int pointer = event_position + channel_number * run_info->M_event_length;
                cout << std::hex << setfill('0') << setw(4) << event.data_stream_matrix[pointer] << " ";
            }
            cout << endl;
        }
        cout << string(100, '-') << endl;
    }
}

void BinaryReader::read_run_footer() {
    // sets data for footer_structure
    file_footer_structure_t * file_footer = (file_footer_structure_t *) (&event);
    run_info->save_footer(file_footer);

    if(debug){
        cout << std::hex << run_info->footer_start << endl;
        cout << std::hex << run_info->total_num_of_events << endl;
        cout << std::hex << run_info->time_stop_a << endl;
        cout << std::hex << run_info->end_footer << endl;
    }

}

bool BinaryReader::check_event_size_that_was_read() {
    if(binary_file->read_buffer_matches_buffer_size()){
        //  we read the either shorter last event or file_footer part
        // first check if read the event or file_footer
        if(event.event_start == event_header){
            // last event was cut off either due to DAQ crash or sth. else
            return false;
        }
        else if (event.event_start == file_footer){
            // we read the files footer so we have to type cast it into the file_footer structure
            read_run_footer();
            return false;
        }
        return false;
    } else
        return true;
    // if lengths match we don't do anything
}

void BinaryReader::convert_time_stamp(){
    // todo make sure that this make sense once I get the physical meaning of it
    // time stamps were written using labView: http://zone.ni.com/reference/en-XX/help/371361H-01/glang/get_date_time_in_seconds/
    // in labview time stamp was converted from double to uint32_t and then back

    current_daq_time_stamp_1 = join_integers(event.time_stamp_daq_1_a, event.time_stamp_daq_1_b);
    current_daq_time_stamp_2 = join_integers(event.time_stamp_daq_2_a, event.time_stamp_daq_2_b);
};

void BinaryReader::load_channel_data_from_stream(unsigned int channel_number){
    // we pass the pointer for the array from point where the channel starts
    uint16_t *start_of_channel_array = event.data_stream_matrix + channel_number*run_info->M_event_length;
    uint16_t *end_of_channel_array = start_of_channel_array + run_info->M_event_length;
    auto storage_iterator =  raw_waveforms.at(channel_number).points.begin();

    std::copy(start_of_channel_array, end_of_channel_array, storage_iterator);

}

void BinaryReader::analyse_event() {

    // read and analise the matrix for every channel in range (num_of_channels)
    for (unsigned int channel_number = 0; channel_number < run_info->N_num_of_channels; ++channel_number) {

        load_channel_data_from_stream(channel_number);

        if (analysis_type == "baseline_analysis")
            baseline_analyser.at(channel_number).analyse_baseline(raw_waveforms.at(channel_number).points);
        else if (analysis_type == "waveform_characteristics_analysis")
            // todo here we also need to load baseline and average of the waveform to to the cuts
            waveform_analyser.at(channel_number).analyse_waveform(channel_number, event_number, raw_waveforms.at(channel_number));

        // plot first n waveforms for veto channel
        if(channel_number == 0 and event_number < num_of_waveforms_from_veto_channel) {
            string file_name = "event_" + to_string(event_number) + "_ch_" + to_string(channel_number);
            string file_path = "plots_final/";//"../../plots/";
            list_plot(file_name, file_path, raw_waveforms.at(channel_number).points);
            ++event_number;
        }

    }

    this->convert_time_stamp();

    run_info->output_root_file->cd();

    // todo baseline analysis and WA
    run_info->baseline_analysis_root_tree->Fill();

    // clean waveform_storage
    for (unsigned int channel_number = 0; channel_number < run_info->N_num_of_channels; ++channel_number) {
        raw_waveforms.at(channel_number).clean_object();
        current_daq_time_stamp_1 = 0;
        current_daq_time_stamp_2 = 0;
    }
}


bool BinaryReader::read_event_data() {
    binary_file->read_buffer(event, current_event_size); // here we first read the event

    if(event.M_event_length != run_info->M_event_length or event.N_num_of_channels != run_info->N_num_of_channels )
        abort_error_massage("Event size and header size of the matrix NXM do not match for event: " + to_string(event.event_number));


    if(check_event_size_that_was_read()){
        if(debug)
            view_event_data();

        if(event.event_number%size_of_analysis_block == 0)
            print_small_separator("Starting reading of event " + to_string(event.event_number) + " ." );

        analyse_event();

        return true;
    }

    return false;
}

void BinaryReader::read_binary_file(){
    // all BinaryReaders use read_buffer(unsigned int num_of_characters)
    read_run_header();

    allocate_memory(); // once we know the sizes of our events we can allocate the memory

    // todo baseline analysis and WA
    create_baseline_analysis_root_tree();

    while(!binary_file->end_of_file_reached){ // read while end_of_file == false
        bool event_successful = read_event_data();

        if(event_successful)
            ++run_info->num_of_successfully_read_events;
    }

    run_info->set_run_info_from_binary_file();
    run_info->store_json_structure_to_root_file();
}

BinaryReader::BinaryReader(unsigned int run_number, string &analysis_type) {
    this->analysis_type = analysis_type;


    this->run_info = shared_ptr<RunInfo>(new RunInfo(run_number));
    this->run_info->create_converted_root_file();

    string binary_file_name = "Acq_run_" + to_string(run_info->run_number);
    string binary_file_path = "binary_files/";
    this->binary_file = shared_ptr<BinaryFile>(new BinaryFile(binary_file_path, binary_file_name));

    this->current_event_size = 0;
    this->event_number = 0;
    this->num_of_waveforms_from_veto_channel = 0;

    read_binary_file();

}

BinaryReader::~BinaryReader() {
    if(destructor_debugging)
        cout << "Cleaning Binary Reader object" << endl;

    // todo check if anything else has to be deleted


    run_info->output_root_file->cd();

    // todo baseline analysis and WA
    run_info->baseline_analysis_root_tree->Write();

    if (analysis_type == "baseline_analysis")
        run_info->output_root_file->Close(); // needed only by baseline_analysis part

    unsigned int first_run_with_correct_footer = 755; // for the rest of the runs we know that the footer is wrong anyway

    if(run_info->num_of_successfully_read_events != run_info->total_num_of_events and
            !run_info->file_footer_missing and run_info->run_number >= first_run_with_correct_footer)
        error_massage("For run " + to_string(run_info->run_number) + ". Number of read events " +
                              to_string(run_info->num_of_successfully_read_events)  + " does not match the number of events "
                      + to_string(run_info->total_num_of_events) + " that should be in the file.");
}
