//
// Created by Ziga Brencic on 13/09/2017.
//

#include "baseline_analysis/RunInfo.h"
#include "support_programs/Flags.h"
#include "baseline_analysis/binary_operations.h"
#include "TObjString.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "support_programs/get_distribution_peak.h"

void RunInfo::create_converted_root_file(){
    output_root_file = new TFile(converted_root_file_name.c_str(),"RECREATE");

    // creating root directories
    if(data_analysis_flags->run_analysis) {
        output_root_file->mkdir("plots");
        output_root_file->mkdir("plots/single_waveforms");
        output_root_file->mkdir("plots/multiple_waveforms");
    }
}

void RunInfo::create_empty_run_json_structure() {
    run_info = R"({})"_json;

    run_info["max_number_of_channels"] = max_num_of_channels;
    run_info["max_number_of_waveform_points"] = max_number_of_waveform_points;
}

void RunInfo::store_json_structure_to_root_file() {
    // saving run info into root file
    std::string temp_str = run_info.dump();
    auto run_info_str = new TObjString(temp_str.c_str()); //smart pointer can't be used here
    output_root_file->WriteObject(run_info_str, "run_info");
}

void RunInfo::set_run_info_from_binary_file() {
    // JSON file construction

    run_info["run_number"] = run_number;
    run_info["run_start_time"] = run_start_time;
    run_info["run_stop_time"] = run_stop_time;
    run_info["stuff"] = stuff;
    run_info["file_footer_missing"] = file_footer_missing;
    run_info["last_event_was_cut"] = last_event_was_cut;
    run_info["num_of_pre_samples"] = num_pre_samples;

    run_info["num_of_empty_settings"] = header_settings_length;
    // store file settings C array into a JSON array
    json settings_array = {};
    for (unsigned int i = 0; i < header_settings_length; ++i)
        run_info["Settings" + to_string(i)] =  settings[i];

    run_info["num_of_events"] = total_num_of_events;
    run_info["num_of_channels"] = N_num_of_channels;
    run_info["event_length"] = M_event_length;
}


void RunInfo::save_header(file_header_structure_t file_header){
    run_start_time = join_integers(file_header.run_start_time_a, file_header.run_start_time_a );
    run_number = file_header.run_number;
    N_num_of_channels = file_header.N_num_of_channels;
    M_event_length = file_header.M_event_length;
    num_pre_samples = file_header.num_pre_samples;
    settings = file_header.settings;

    num_of_pre_samples_for_baseline = num_pre_samples/2;

}
void RunInfo::save_footer(file_footer_structure_t *file_footer){
    footer_start = file_footer->footer_start;
    total_num_of_events = file_footer->total_num_of_events;
    run_stop_time = join_integers(file_footer->time_stop_a, file_footer->time_stop_b);
    stuff = file_footer->stuff;
    end_footer = file_footer->end_footer;
}

void RunInfo::load_channel_data(string &channel_characteristic, unsigned int channel_number, bool debug){
    string run_file = "converted_runs/Converted_run_" +  to_string(run_number)+ ".root";
    string ch_data = "ch_" + to_string(channel_number) + "_" + channel_characteristic;
    auto converted_run = shared_ptr<TFile>(new TFile(run_file.c_str()));
    auto data_tree = (TTree *)gDirectory->Get("baseline_analysis_root_tree");

    auto canvas = shared_ptr<TCanvas>(new TCanvas("baseline_canvas"));

    string draw_command = ch_data + ">>" + channel_characteristic;
    data_tree->Draw(draw_command.c_str());

    auto baseline_histogram = (TH1F*)gPad->GetPrimitive(channel_characteristic.c_str());

    if(channel_number == 13 and channel_characteristic == "baseline") {
        distribution_peak_t peak = get_distribution_peak(baseline_histogram);
        run_info[channel_characteristic][channel_number]["mean"] = peak.x;
    }
    else
        // storing std_dev and mean to the run info map.
        run_info[channel_characteristic][channel_number]["mean"] = baseline_histogram->GetMean(1);

    run_info[channel_characteristic][channel_number]["std_dev"] = baseline_histogram->GetStdDev(1);

    if(debug) {
        cout << "Mean: " << run_info[channel_characteristic][channel_number]["mean"] << endl;
        cout << "StdDev: " << run_info[channel_characteristic][channel_number]["std_dev"] << endl;

        canvas->SetGrid();
        canvas->Modified();
        canvas->Update();
        string name = "plots_final/run_" + to_string(run_number) + "_" + ch_data + ".pdf";

        canvas->SaveAs(Form("%s", (name).c_str()));
    }

};

RunInfo::RunInfo(unsigned int run_number) : file_header_structure_t(), file_footer_structure_t() {
    this->run_number = run_number;
    this->converted_root_file_name = "converted_runs/Converted_run_" + to_string(run_number) + ".root";
    this->processed_root_file_name = "processed_runs_final/Processed_run_" + to_string(run_number) + ".root";

    this->data_analysis_flags = shared_ptr<Flags>(new Flags());

    this->num_of_successfully_read_events = 0;

    this->file_footer_missing = false;
    last_event_was_cut = false;

    run_info["baseline"] = baseline;
    run_info["waveform_average"] = waveform_average;

    create_empty_run_json_structure();

}

RunInfo::~RunInfo() {
    if(destructor_debugging)
        cout << "Deleting Run Info object." << endl;
}
