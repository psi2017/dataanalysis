//
// Created by Ziga Brencic on 09/09/2017.
//

#include "baseline_analysis/Waveform.h"


void Waveform::clean_object() {
    std::fill(points.begin(), points.end(), 0);
    this->baseline = 0;
    this->waveform_average = 0;
}

Waveform::Waveform(unsigned int waveform_length) {
    this->points.assign(waveform_length, 0);
    this->baseline = 0;
    this->waveform_average = 0;
}

Waveform::~Waveform() = default;
