//
// Created by Ziga Brencic on 05/12/2017.
//

#include "baseline_analysis/binary_operations.h"


uint64_t join_integers(uint32_t leastSignificantWord, uint32_t mostSignificantWord){
    return (uint64_t) mostSignificantWord << 32 | leastSignificantWord;
}