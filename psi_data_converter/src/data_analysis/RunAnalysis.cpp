//
// Created by Ziga Brencic on 08/09/2017.
//

#include "data_analysis/RunAnalysis.h"
#include "waveform_analysis/WaveformAnalysis.h"

#include "baseline_analysis/RunInfo.h"
#include "support_programs/IO.h"

// todo incorporate properly
//void BinaryReader::set_run_analysis_class(){
//    run_analysis = shared_ptr<RunAnalysis>(new RunAnalysis(run_info->output_root_file, run_info->run_number, run_info->N_num_of_channels, run_info->M_event_length));
//    run_analysis->create_storage_for_plot_all_waveforms();
//}

void RunAnalysis::store_event_waveforms(vector<WaveformAnalysis> event_waveforms, unsigned int event_number) {


    if(!storage_for_all_waveforms_created)
        create_storage_for_plot_all_waveforms();

    shared_ptr<vector<Double32_t>> temp_waveform;
    TGraph *temp_TGraph;

    print_small_separator("Saving waveform " + to_string(event_number) + ".");

    for (unsigned int channel_number = 0; channel_number < num_of_channels ; ++channel_number){
        // storing event data
        vector<uint16_t> pointer = event_waveforms.at(channel_number).waveform_points;

        temp_waveform = shared_ptr<vector<Double32_t>> (new vector<Double32_t> (pointer.begin(), pointer.end()));
        matrix_of_waveform_vectors.at(channel_number).push_back(temp_waveform); // storing the pointer vector<Double32_t>

        // store the graph for each waveform
        temp_TGraph = new TGraph(event_length, &list_plot_points[0], &(*temp_waveform)[0]);
        matrix_of_TGraphs.at(channel_number).push_back(temp_TGraph); // storing the pointer to TGraph

        temp_TGraph->SetLineColorAlpha(kRed, 0.05);

        for (unsigned int i = 0; i < event_length; ++i) {
            waveform_histograms.at(channel_number)->Fill(i, temp_waveform->at(i));
            waveform_histograms_zoom.at(channel_number)->Fill(i, temp_waveform->at(i));
        }

        channel_plots.at(channel_number)->Add(temp_TGraph);
    }
    ++num_of_events_analysed;
}

void RunAnalysis::create_storage_for_plot_all_waveforms() {

    for (Double32_t i = 0; i < event_length; ++i)
        list_plot_points.push_back(i);

    matrix_of_waveform_vectors.assign(num_of_channels, vector<shared_ptr<vector<Double32_t>>>());
    matrix_of_TGraphs.assign(num_of_channels, vector<TGraph *> ());

    for (unsigned int channel_number = 0; channel_number < num_of_channels; ++channel_number) {
        channel_plots.push_back(new TMultiGraph());
        string histogram_name = "channel_" + to_string(channel_number);
        string histogram_title = "histogram_" + histogram_name;
        TH2D *temp_hist = new TH2D(/* name */ histogram_name.c_str(),
                /* title */ histogram_title.c_str(),
                /* X-dimension */ event_length/2, 0, event_length,
                /* Y-dimension */ 200, 7500.0, 12000.0);

        histogram_name = histogram_name + "_zoom";

        TH2D *temp_hist_zoom = new TH2D(/* name */ histogram_name.c_str(),
                /* title */ histogram_title.c_str(),
                /* X-dimension */ event_length/2, 0, event_length,
                /* Y-dimension */ 50, 8100.0, 8300.0);

        waveform_histograms.push_back(temp_hist);
        waveform_histograms_zoom.push_back(temp_hist_zoom);

    }

    storage_for_all_waveforms_created = true;
    plot_all_waveforms = true; // we set this flag to true so we save the generated plots once the class destructor is called
}

void RunAnalysis::save_plot_of_all_waveforms() {

    // todo add option save as pdf



    for (unsigned int channel_number = 0; channel_number < num_of_channels ; ++channel_number) {
        // saving all waveforms

        string canvas_name = "ch_" + to_string(channel_number);
        string canvas_title = "Waveforms " + canvas_name;
        TCanvas *temp_canvas = new TCanvas(canvas_name.c_str(), canvas_title.c_str(), canvas_width, canvas_height);
        temp_canvas->SetGrid();

        TMultiGraph *temp_graph = channel_plots.at(channel_number);

        temp_graph->Draw("A");
        temp_canvas->Modified();
        temp_canvas->Update();

        string file_name = "Run_" + to_string(run_number) + "_waveforms_ch_" + to_string(channel_number);
        string temp_name = "waveforms_channel_" + to_string(channel_number);

        output_root_file->cd("plots/multiple_waveforms");
        temp_graph->Write(temp_name.c_str());

//        string name = plots_folder_path + file_name + ".pdf";
//        temp_canvas->SaveAs(Form("%s", (name).c_str()));

        delete temp_canvas;

        // saving all waveform histograms
        canvas_name = "ch_" + to_string(channel_number) + "_histogram_of";
        canvas_title = "_waveforms " + canvas_name;
        TCanvas *temp_canvas_histogram = new TCanvas(canvas_name.c_str(), canvas_title.c_str(), canvas_width, canvas_height);
        temp_canvas_histogram->SetGrid();

        TH2D *temp_histogram = waveform_histograms.at(channel_number);

        temp_histogram->Draw("COLZ");
        temp_canvas_histogram->Modified();
        temp_canvas_histogram->Update();

        file_name = "Run_" + to_string(run_number) + "_histogram_of_waveforms_ch_" + to_string(channel_number);
//        name = plots_folder_path + file_name + ".pdf";
//        temp_canvas_histogram->SaveAs(Form("%s", (name).c_str()));
        temp_name = "histogram_channel_" + to_string(channel_number);

        output_root_file->cd("plots/multiple_waveforms");
        temp_histogram->Write(temp_name.c_str());

        delete temp_canvas_histogram;

        // save all zoomed histograms
        canvas_name = "ch_" + to_string(channel_number) + "_histogram_of";
        canvas_title = "_waveforms_zoom " + canvas_name;
        TCanvas *temp_canvas_histogram_zoom = new TCanvas(canvas_name.c_str(), canvas_title.c_str(), canvas_width, canvas_height);
        temp_canvas_histogram_zoom->SetGrid();

        TH2D *temp_histogram_zoom = waveform_histograms_zoom.at(channel_number);

        temp_histogram_zoom ->Draw("COLZ");
        temp_canvas_histogram_zoom->Modified();
        temp_canvas_histogram_zoom->Update();

        file_name = "Run_" + to_string(run_number) + "_histogram_of_waveforms_zoom_ch_" + to_string(channel_number);
//        name = plots_folder_path + file_name + ".pdf";
//        temp_canvas_histogram_zoom->SaveAs(Form("%s", (name).c_str()));

        output_root_file->cd("plots/multiple_waveforms");
        temp_histogram_zoom->Write(file_name.c_str());

        delete temp_canvas_histogram_zoom;


    }
}

void RunAnalysis::create_plot_of_all_waveforms_from_root_file() {
    // todo  read event from root_file

    //    for event in root_tree:
    //    store_event_waveforms()
    //    save_plot_of_all_waveforms();
}

RunAnalysis::RunAnalysis(TFile *input_root_file, unsigned int input_run_number, unsigned int input_num_of_channels, unsigned int input_event_length) {
    // todo move as much as possible to RunInfo and load it
    // run_info = shared_ptr<RunInfo> (new RunInfo());
    data_analysis_flags = shared_ptr<Flags> (new Flags());

    output_root_file = input_root_file;
    run_number = input_run_number;
    num_of_channels = input_num_of_channels;
    num_of_events_analysed = 0;
    event_length = input_event_length;

    plots_folder_path = "plots/";
//    canvas_width = 1600;
//    canvas_height = 600;


    storage_for_all_waveforms_created = false;



    if(plot_all_waveforms)
        create_plot_of_all_waveforms_from_root_file();



}

RunAnalysis::~RunAnalysis() {
    if(data_analysis_flags->destructor_debugging)
        cout << "Cleaning Run Analysis object" << endl;

    // todo check that class destructor is correct: make sure to delete all new Object things
//    delete output_root_file;

    if(plot_all_waveforms){
        save_plot_of_all_waveforms();

        // delete matrix_of_waveform_vectors;
//        for (unsigned int event = 0; event < matrix_of_waveform_vectors.at(0).size(); ++event) {
//            for (unsigned int channel_number = 0; channel_number < num_of_channels; ++channel_number) {
//                delete matrix_of_waveform_vectors.at(channel_number).at(event);
//                matrix_of_waveform_vectors.at(channel_number).at(event) = NULL;
//
//            }
//        }
    }

//    delete data_analysis_flags;
//    data_analysis_flags = NULL;

}