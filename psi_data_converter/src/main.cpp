#include "Analyst.h"
#include <iostream>
#include "support_programs/definitions.h"
#include "TROOT.h"


/// how to use the converter:
/// step 1:  ./psi_data_converter 753 754 baseline_analysis
/// step 2: ./psi_data_converter 753 754 waveform_analysis

int main(int argc, char** argv){
    if(argc <= 3) {
        std::cout << "USAGE: baseline_analysis min_run_num max_run_num analysis_type" << std::endl;
        std::cout << "This program has to be run in the data folder (~/data/preliminary)." << std::endl;
        std::cout << "The binary files are located in binary_files." << std::endl;
        return 1;
    }

    // todo function that returns the input arguments
    char* end_ptr; // used to with strtol instead of atoi to catch errors while loading input arguments
    auto start_run_number = (unsigned int )strtol(argv[1], &end_ptr, 10);
    auto last_run_number = (unsigned int )strtol(argv[2], &end_ptr, 10);
    string analyst_flag = argv[3];

    bool rewrite_files = true;

    for(unsigned int run_number = start_run_number; run_number < last_run_number; ++run_number) {
        gROOT->Reset("a"); // resetting root global environment
        // "a" is set to reset all startup context (i.e. unload also all loaded files, classes, structs, typedefs, etc.).

        if (analyst_flag == "baseline_analysis") {
            auto run_analyst = shared_ptr<Analyst>(new Analyst(run_number, analyst_flag));
            run_analyst->execute_binary_reader(rewrite_files);
        }

        if (analyst_flag == "waveform_analysis") {
            auto run_analyst = shared_ptr<Analyst>(new Analyst(run_number, analyst_flag));
            run_analyst->execute_waveform_analysis(rewrite_files);
        }
    }
};
