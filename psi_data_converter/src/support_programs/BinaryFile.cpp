//
// Created by Ziga Brencic on 13/09/2017.
//

#include "support_programs/BinaryFile.h"

void BinaryFile::open_file() {
    // we open a specific binary file
    stream.open(file_path + file_name, std::ios::in | std::ios::binary);

    print_small_separator("Opening file: " + file_name);

    if (!stream.is_open()) {
        // without an open binary file we print error massage and abort
        abort_error_massage("Failed to open file: " + file_name);
    }
}

void BinaryFile::close_file(){
    if(debug)
        cout << string(100, '-')  << endl;


    print_small_separator("Closing file: " + file_name);
    stream.close();
}

void BinaryFile::is_end_of_file_reached() {
    // if yes it sets global end_of_file_reached to true
    if(!stream.good()) {
        if(debug)
            print_small_separator("End of file reached!");

        end_of_file_reached = true;
    }
}

bool BinaryFile::read_buffer_matches_buffer_size() {
    return (this->stream.gcount() != this->buffer_size);
}

BinaryFile::BinaryFile(string file_path, string file_name) {
    this->file_path = file_path;
    this->file_name = file_name + ".bin";
    this->end_of_file_reached = false; // by default the end of file was not reached yet

    open_file();

}

BinaryFile::~BinaryFile() {
    close_file();
    if(destructor_debugging)
        cout << "Deleting Binary File object." << endl;

//    delete data_analysis_flags;
//    data_analysis_flags = NULL;
}
