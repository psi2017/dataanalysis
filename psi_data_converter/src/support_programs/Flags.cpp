//
// Created by Ziga Brencic on 13/09/2017.
//

#include "support_programs/Flags.h"
#include "support_programs/definitions.h"


//#define DEBUG
//#define EVENT_BIN_FORMAT // turn on the data for event
//#define RUN_ANALYSIS

#define ANALYSIS
//#define DESTRUCTOR_DEBUGGING
//#define GET_PDF



Flags::Flags() {
    waveform_analysis = false;
    debug = false;
    event_bin_format = false;
    run_analysis = false;
    destructor_debugging = false;
    get_pdf = false;

    #ifdef DEBUG
        debug = true;
    #endif

    #ifdef EVENT_BIN_FORMAT
        event_bin_format = true;
    #endif

    #ifdef RUN_ANALYSIS
        run_analysis = true;
    #endif

    #ifdef ANALYSIS
        waveform_analysis = true;
    #endif

    #ifdef DESTRUCTOR_DEBUGGING
        destructor_debugging = true;
    #endif

    #ifdef GET_PDF
        get_pdf = true;
    #endif
}

Flags::~Flags() {
    if(destructor_debugging)
        cout << "Deleting Flags object." << endl;
}
