//
// Created by Ziga Brencic on 05/09/2017.
//

#include "support_programs/IO.h"

unsigned int separator_size = 100;

void massage(string massage_content, char massage_mark ){
    cout << string(separator_size, massage_mark) << endl;
    cout << massage_content << endl;
    cout << string(separator_size, massage_mark)  << endl;
}

void print_big_separator(string input_string){
   massage(input_string, '=');
}

void print_small_separator(string input_string){
    cout << input_string << endl;
    cout << string(separator_size, '-')  << endl;
}


void error_massage(string output){
    massage(output, '!');
}


void abort_error_massage(string output){
    massage(output, '!');
    abort();
}
