//
// Created by Marius Koeppel on 10/09/2017.
//

#include "support_programs/RootReader.h"
#include "TMacro.h"
#include "support_programs/IO.h"
#include "support_programs/Flags.h"

void RootReader::open_root_file_and_connect_to_root_tree(){
    string root_file_name = "processed_runs/Processed_run_" + to_string(run_number) + ".root";
    root_file = new TFile(root_file_name.c_str());
    root_tree = (TTree*)root_file->Get("root_tree");
}


void RootReader::set_branch_addresses(){
    for (unsigned int channel_number = 0; channel_number < num_of_channels; channel_number++) {
        string branch_name = "branch_ch_" + to_string(channel_number);
        root_tree->SetBranchAddress(branch_name.c_str(), &event_waveforms.at(channel_number)[0]);
    }
}


void RootReader::set_aliases(){
//    tree->SetAlias("x1","(tdc1[1]-tdc1[0])/49");
}

void RootReader::read_from_root_file(){
    unsigned long int num_of_events = (unsigned long int)root_tree->GetEntries(); // todo read from root TObject
    for (unsigned  int entry = 0; entry < 1; entry++) {
        root_tree->GetEntry(entry);
        cout << "test length " << event_waveforms.at(0).size() << endl;

        for (unsigned int i = 0; i < event_waveforms.at(0).size(); ++i) {
//            cout << std::hex << event_waveforms.at(0).at(i) << endl;
        }
    }

    // todo set event number;
//        }
//    }

}

void RootReader::memory_allocation(){
//    event_waveforms.reserve(num_of_channels); // reserve space for the vector of WaveformAnalysis objects
//
//     create object for every entry of event_waveforms vector
//    for (int i = 0; i < num_of_channels; ++i) {
//        event_waveforms.push_back(vector<UShort_t >());
//    }
    for (unsigned int channel_number = 0; channel_number < num_of_channels; ++channel_number) {
        event_waveforms.push_back(vector<UShort_t> ());
        event_waveforms.at(channel_number).assign(event_length, 0);
    }
}

void RootReader::print_run_info(){
    print_small_separator("Run info");
    for (auto it = run_info.begin(); it != run_info.end(); ++it) {
        if (it.key().find("Settings") != std::string::npos) {
            // we don't display the settings because they are currently empty
        } else
            std::cout <<  setfill(' ') << setw(35) << it.key() << " | " << it.value() << "\n";
    }
    cout << string(100, '-')  << endl;
}

void RootReader::read_run_info(){
    TObjString *temp_str = (TObjString *)root_file->Get("run_info");

    if(temp_str == NULL){
        error_massage("Error reading run info of run " + to_string(run_number));

    }else {
        string run_info_str = temp_str->GetString().Data();
        run_info = json::parse(run_info_str);

        // todo catch error massages if any of elements in dictonary does not exist

        max_num_of_channels = run_info["max_number_of_channels"];
        max_number_of_waveform_points = run_info["max_number_of_waveform_points"];

        time_start = run_info["run_start_time"];
        time_stop = run_info["run_stop_time"];
        stuff = run_info["stuff"];
        file_footer_missing = run_info["file_footer_missing"];
        last_event_was_cut = run_info["last_event_was_cut"];
        num_of_pre_samples = run_info["num_of_pre_samples"];

        num_of_events = run_info["num_of_events"];
        num_of_channels = run_info["num_of_channels"];
        event_length = run_info["event_length"];

        // todo uncomment if we start using settings
//        unsigned int header_settings_length = run_info["num_of_empty_settings"];
//        // store file settings C array into a JSON array
//        json settings_array = {};
//        for (unsigned int i = 0; i < header_settings_length; ++i)
//            run_info["Settings" + to_string(i)];

        print_run_info();
    }
}

RootReader::RootReader(unsigned int input_run_number) {
    debug_mode = false;

    run_number = input_run_number;
    print_big_separator("Analysing run " + to_string(run_number));

    open_root_file_and_connect_to_root_tree();
    read_run_info();


    memory_allocation();

    read_from_root_file();

}

RootReader::~RootReader() {
    root_file->Close();
    if(destructor_debugging)
        cout << "Cleaning Root Reader object" << endl;

}
