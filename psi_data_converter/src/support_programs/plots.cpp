//
// Created by Ziga Brencic on 16/01/2018.
//

#include "support_programs/plots.h"

#include "TGraph.h"
#include "TCanvas.h"

void list_plot(const string &plot_name, const string &plot_path, vector<uint16_t> points){
// TODO should be able to except any data type in vector

    vector<Double32_t> list_plot_points;
    list_plot_points.assign(points.size(), 0);
    for(unsigned int i = 0; i < points.size(); ++i)
        list_plot_points.at(i) = i;

    vector<Double32_t> temp_points(points.begin(), points.end());
    auto data_set_length = (unsigned int)points.size();

    auto plot =  shared_ptr<TGraph>(new TGraph(data_set_length, &list_plot_points[0], &temp_points[0]));

    plot->Write(plot_name.c_str());

    auto canvas = shared_ptr<TCanvas>(new TCanvas("c1"));
    plot->Draw("AL");

    canvas->SetGrid();
    canvas->Modified();
    canvas->Update();
    string name = plot_path + plot_name + ".pdf";
    canvas->SaveAs(Form("%s", (name).c_str()));
}
