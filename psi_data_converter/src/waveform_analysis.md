# Waveform analysis

## Root tree structure
- ch_0_peak_positions => vector<unint16_t>
- ch_(1-13)_rise_edges => vector<float>
- ch_(1-13)_peak_positions => vector<uint16_t>
- ch_(7-8)_integrals => vector<float>

## Waveform structure:

structure Waveform{ \
    vector<float> rise_edges; \
    vector<uint16_t> peak_positions;\
    vector<float> fall_edges;\
    vector<float> integrals;\
};

## Algorithm

- TODO: check larses code if it stores the vectors correctly => I need root reader   
- TODO: code clean up

### Baseline analysis
We take half of the pre samples for each waveform and calculate their average to get baseline estimate.

Then we calculate the average of each waveform to get estimate on how much signal we have in each
waveform. This allows us to later throw out flat waveforms that out current analysis algorithm isn't capable 
of analysing correctlly. 

