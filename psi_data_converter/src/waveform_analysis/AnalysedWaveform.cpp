//
// Created by Ziga Brencic on 22/11/2017.
//

#include "waveform_analysis/AnalysedWaveform.h"

AnalysedWaveform::AnalysedWaveform(unsigned int input_N_num_of_waveform_points) {
    points.assign(input_N_num_of_waveform_points, 0);
    derivatives.assign(input_N_num_of_waveform_points, 0);

    peak_height.assign(input_N_num_of_waveform_points, 0);
    peak_position.assign(input_N_num_of_waveform_points, 0);
    rise_edge_position.assign(input_N_num_of_waveform_points, 0);
    fall_edge_position.assign(input_N_num_of_waveform_points, 0);

    noise = 0;

    integral.assign(input_N_num_of_waveform_points, 0);
    integration_interval.assign(input_N_num_of_waveform_points, 0);

    flat = false;
    rise_edge_problem = false;
    fall_edge_problem = false;
    multiple_peaks = false;
}

AnalysedWaveform::~AnalysedWaveform() {

}