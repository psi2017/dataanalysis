//
// Created by Ziga Brencic on 01/09/2017.
//

#include "waveform_analysis/WaveformAnalysis.h"
#include "support_programs/IO.h"

#include "TTree.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TH1.h"
#include "baseline_analysis/RunInfo.h"
#include "support_programs/Flags.h"
#include "baseline_analysis/Waveform.h"

unsigned int num_of_plotted_events = 0;

void WaveformAnalysis::calculate_channel_baseline(unsigned int num_of_pre_samples_for_baseline){
    // calculated from binary reader
    baseline = 0;

    for (uint16_t point = 0; point < num_of_pre_samples_for_baseline ; ++point)
        baseline += waveform_points.at(point);

    baseline = baseline/(num_of_pre_samples_for_baseline);

}

void WaveformAnalysis::set_channel_baseline(unsigned int num_of_pre_samples_for_baseline) {

    // if Proccessed_run_[].root already exist use this (if Waveform analysis is run separatly)

    // if Waveform analysis is called from BinaryReader the we caluclate the baseline based on first half pre samples
    calculate_channel_baseline(num_of_pre_samples_for_baseline);

    //if( baseline > average_channel_baseline)
}

void WaveformAnalysis::calculate_waveform_derivatives(){
    // calculated from binary reader
    for (unsigned int point = 0; point < waveform_derivatives.size(); ++point)
        waveform_derivatives.at(point) = waveform_points.at(point + 1) - waveform_points.at(point);
}

// With the noise we calculating the rise_edge
uint64_t WaveformAnalysis::find_rise_edge_by_threshold() {
    // Here we choose a fix threshold we changed it the best was 20
    uint64_t threshold = 20, rising_edge = 0;  // todo fix this hard coding
    rising_edge = 0;
    for (unsigned int i = 0; i < waveform_points.size(); ++i) {
        if (waveform_points.at(i) > baseline + threshold) return rising_edge;
        else ++rising_edge;
    }
    return 0;
}

// We choosed a fix interval for the integral
uint64_t WaveformAnalysis::find_integral_by_fix_window(uint64_t rising_edge){
    // Use a fix integration window the best was 100
    uint64_t integral_window = 0;
    int window = 100;
    if(channel_number > 8) // todo fix this hard coding
        window = 10;
    uint64_t fall = rising_edge + window;
    if(fall > waveform_points.size())
        fall = waveform_points.size();

    uint64_t integration_interval = fall - rising_edge;
    for (uint64_t position = rising_edge; position < fall; ++position) {
        integral_window += waveform_points.at(position);
    }
    if (integration_interval * baseline > integral_window)
        return 0;
    integral_window = integral_window - integration_interval * baseline;
    return integral_window;
}

// this is from Lukas but it is also working well
void WaveformAnalysis::find_amplitude() { // todo this works
    peak_height = *std::max_element(waveform_points.begin(), waveform_points.end());
}


uint64_t WaveformAnalysis::integration_width_trapezoidal_method(){
    // todo check how method works
    unsigned int window_size = 50;
    uint64_t sum0 = 0;
    uint64_t sum;
    vector<uint64_t> sum_array;

    for (unsigned int j=0; j< (window_size); j++){
            sum0 += waveform_points.at(j);
        }
    sum_array.push_back(sum0);

    for (unsigned int i=1; i< (waveform_points.size()-window_size); i++){
           // sum_array[i-1]        
        sum = sum0 + waveform_points.at(window_size + (i-1)) - waveform_points.at(i-1);
        sum_array.push_back(sum);
        sum0 = sum;
    }

   uint64_t min= *std::min_element(sum_array.begin(),sum_array.end());
   uint64_t max= *std::max_element(sum_array.begin(),sum_array.end());
    uint64_t integral_via_trapezoidal_method = (max > min) ? max - min : min - max;
   return integral_via_trapezoidal_method;
}


void WaveformAnalysis::process_peaks_by_DOS(){
    // todo
}

void WaveformAnalysis::process_peaks_by_noise_threshold(){
    // todo
}


void WaveformAnalysis::multiple_peak_finder(){
    set_channel_baseline(run_info->num_of_pre_samples_for_baseline);

    // cut off finder after calo finishes

    // Integration with fix threshold
    // todo find all peaks
//    set_channel_noise();
    uint64_t rise_edge_th = find_rise_edge_by_threshold();
    uint64_t integral_th = find_integral_by_fix_window(rise_edge_th);

    find_amplitude();

}



void WaveformAnalysis::plot_waveform(vector<Double_t> list_plot_points) { // todo save that stuff to root file

    vector<Double32_t> temp_waveform_points(waveform_points.begin(), waveform_points.end());
    auto waveform_length = (unsigned int) waveform_points.size();

    waveform_plot =  shared_ptr<TGraph>(new TGraph(waveform_length, &list_plot_points[0], &temp_waveform_points[0]));

    string file_name = "event_" + to_string(event_number) + "_ch_" + to_string(channel_number);
    // todo fix: output_root_file is never specified in this class

    output_root_file->cd("plots/single_waveforms");
    waveform_plot->Write(file_name.c_str());



    if(data_analysis_flags->get_pdf){
        TCanvas *canvas = new TCanvas();
        waveform_plot->Draw("AL");

        canvas->SetGrid();
        canvas->Modified();
        canvas->Update();
        string file_path = "plots/";//"../../plots/";
        string name = file_path + file_name + ".pdf";
//        canvas->SaveAs(Form("%s", (name).c_str()));
        gDirectory->Write(name.c_str());
    }
    ++num_of_plotted_events;
}


void WaveformAnalysis::clear_storage(){
    std::fill(waveform_points.begin(), waveform_points.end(), 0);
    std::fill(waveform_derivatives.begin(), waveform_derivatives.end(), 0);
}

void print_waveform_characteristics(){
    cout << "Overview of waveform characteristics" << endl;
//    num_of_peaks = rising_edge.size();
//    std::cout << "Channel " << channel_number << '\n';
//    std::cout << "" << '\n';
//    std::cout << "  baseline at " << noise << '\n';
//    std::cout << "  threshold at " << absolute_threshold << '\n';
//    //std::cout << "  " << fraction << " threshold at " << new_threshold << '\n';
//    std::cout << "  found " << rising_edge.size() << " Peaks:" << '\n';
//    if (rising_edge.size() != 0){
//        for (unsigned int peak_number = 0; peak_number < rising_edge.size(); peak_number++) {
//            std::cout << "    Peak " << peak_number << " from " << rising_edge.at(peak_number) << " to " << falling_edge.at(peak_number) << " with TOT " << TOT.at(peak_number) << " and integral of " << integral.at(peak_number) << '\n';
//            if (peak_number < rising_edge_fine.size()) {
//                std::cout << "      fine rising edge at " << rising_edge_fine.at(peak_number) << '\n';
//            }
//        }
//    }
}

void WaveformAnalysis::analyse_waveform(unsigned int channel_number, unsigned int event_number, Waveform &raw_waveform) {
    this->channel_number = channel_number;
    this->event_number = event_number;

    multiple_peak_finder();

    // todo print_waveform_characteristics();

}

WaveformAnalysis::WaveformAnalysis(shared_ptr<RunInfo> run_info) {
    this->run_info = run_info;
    this->data_analysis_flags = shared_ptr<Flags>(new Flags());

    this->waveform_points.assign(run_info->M_event_length, 0);
    this->waveform_derivatives.assign(run_info->M_event_length - 1, 0); // we have one less derivative than we have the points


    // todo move all the flags to run info or to flag object


}

WaveformAnalysis::~WaveformAnalysis() {
    if(destructor_debugging)
        cout << "Deleting Waveform Analysis object." << endl;

    // todo check that all objects are deleted

//    delete data_analysis_flags;
//    data_analysis_flags = NULL;


}
