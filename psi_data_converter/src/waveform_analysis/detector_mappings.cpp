//
// Created by Ziga Brencic on 05/12/2017.
//

#include "waveform_analysis/detector_mappings.h"
#include "TString.h"
#include "support_programs/definitions.h"

void map_detectors(){

    // mapping of dectors is the same trough whole experiment

    vector<TString> detectors({"Veto", "Bcalo1", "Bcalo2", "Bcalo3", "Bcalo4", "Bcalo5", "Bcalo6", "Scalo", "SumBcalo", "ScB1", "ScB2", "ScS1", "ScS2", "notBCM"});
    vector<TString> data_types({"amount_of_peaks", "peak_height", "rising_edge", "falling_edge", "TOT", "integral", "rising_edge_fine"});

    cout << "Num of detectors: " << detectors.size() << endl;

    std::map<TString, int> channel_mapping;

    for(unsigned int channel_number = 0; channel_number < (detectors.size() - 1); ++channel_number)
        channel_mapping[detectors.at(channel_number)] = channel_number;

    map<TString, map<TString, TString>> data; // example how to get entry: data["Veto"]["data"]
    // peak_height leaf (in ADC units)
    // rising_edge leaf (in units of 10 ns)
    // falling_edge leaf (in units of 10 ns)
    // TOT leaf (in units of 10 ns)
    // rising_edge_fine leaf (in units of 1 ns)
    // integral leaf (to be calibrated in macros)


    for(unsigned int channel_number = 0; channel_number < (detectors.size() - 1); ++channel_number){
        TString ch = (TString)("ch" + to_string(channel_number) + "_");
        TString detector = (TString)detectors.at(channel_number);
        for (size_t type_number = 0; type_number < (data_types.size() - 1); type_number++) {
            TString data_type = (TString)data_types.at(type_number);
            data[detector][data_type] = ch + data_type;
        }
    }

    // todo fix soffset, sratio and same for big calo
//    data["Scalo"]["integral"] = Form("((ch7_integral - %f) * %f)", soffset, sratio);
//    data["SumBcalo"]["integral"]  = Form("((ch8_integral - %f) * %f)", boffset, bratio);

    for (size_t channel_number = 0; channel_number < (detectors.size() - 1); channel_number++) {
        for (size_t type_number = 0; type_number < (data_types.size() - 1); type_number++)
            cout << data[detectors.at(channel_number)][data_types.at(type_number)] << "| ";
        cout << endl;
    }
}